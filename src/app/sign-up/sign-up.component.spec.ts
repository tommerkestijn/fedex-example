import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import {HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SignUpComponent } from './sign-up.component';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SignUpComponent,
      ],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        FormBuilder,
        SignUpComponent,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .overrideComponent(SignUpComponent, {
      set: {
        providers: []
      }
    })
    .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SignUpComponent);
        component = fixture.componentInstance;
        component.ngOnInit();
        httpMock = TestBed.get(HttpTestingController);
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.form.valid).toBeFalsy();
  });

  it('firstName field should be not valid', () => {
    let firstName = component.form.controls['firstName'];

    expect(firstName.valid).toBeFalsy();
  });

  it('firstName field should be valid', () => {
    component.form.controls['firstName'].setValue('foo');
    let firstName = component.form.controls['firstName'];

    expect(firstName.valid).toBeTruthy();
  });

  it('lastName field should be not valid', () => {
    let lastName = component.form.controls['lastName'];

    expect(lastName.valid).toBeFalsy();
  });

  it('lastName field should be valid', () => {
    component.form.controls['lastName'].setValue('foo');
    let lastName = component.form.controls['lastName'];

    expect(lastName.valid).toBeTruthy();
  });

  it('email field should be not valid', () => {
    let email = component.form.controls['email'];

    expect(email.valid).toBeFalsy();
  });

  it('email field should be valid', () => {
    component.form.controls['email'].setValue('foo@baz.com');
    let email = component.form.controls['email'];

    expect(email.valid).toBeTruthy();
  });

  it('password field should be not valid, if empty', () => {
    let password = component.form.controls['password'];

    expect(password.valid).toBeFalsy();
  });

  it('password field should be not valid with 6 characters', () => {
    component.form.controls['password'].setValue('Abcdef');
    let password = component.form.controls['password'];

    expect(password.valid).toBeFalsy();
  });

  it('password field should be not valid without an uppercase character', () => {
    component.form.controls['password'].setValue('abcdefgh');
    let password = component.form.controls['password'];

    expect(password.valid).toBeFalsy();
  });

  it('password field should be not valid if the firstName includes the password', () => {
    component.form.controls['firstName'].setValue('foo');
    component.form.controls['password'].setValue('fooAbcdef');
    let password = component.form.controls['password'];

    expect(password.valid).toBeFalsy();
  });

  it('password field should be not valid if the lastName includes the password', () => {
    component.form.controls['lastName'].setValue('foo');
    component.form.controls['password'].setValue('fooAbcdef');
    let password = component.form.controls['password'];

    expect(password.valid).toBeFalsy();
  });

  it('password confirmation should not be valid if it is not the same as the first password', () => {
    component.form.controls['password'].setValue('Abcdefgh123');
    component.form.controls['confirmPassword'].setValue('Abcdefgh555');
    let password = component.form.controls['confirmPassword'];

    expect(password.valid).toBeFalsy();
  });

  it('password confirmation should be valid if it is the same as the first password', () => {
    component.form.controls['password'].setValue('Abcdefgh123');
    component.form.controls['confirmPassword'].setValue('Abcdefgh123');
    let password = component.form.controls['confirmPassword'];

    expect(password.valid).toBeTruthy();
  });

  it('should do a succesfull request and set signUpSuccess to true', () => {
    expect(component.form.valid).toBeFalsy();

    component.form.controls['firstName'].setValue('foo');
    component.form.controls['lastName'].setValue('baz');
    component.form.controls['email'].setValue('test@test.com');
    component.form.controls['password'].setValue('ABc123456789');
    component.form.controls['confirmPassword'].setValue('ABc123456789');

    expect(component.form.valid).toBeTruthy();

    component.onSubmit();

    const req = httpMock.expectOne('https://demo-api.now.sh/users');
    const expectedResponse = new HttpResponse({ status: 200, statusText: 'Created', body: {} });
    req.event(expectedResponse);

    expect(req.request.method).toEqual('POST');
    expect(component.signUpSuccess).toBe(true);

    httpMock.verify();
  });

  it('should not do a succesfull request and set signUpInvalid to true', () => {
    expect(component.form.valid).toBeFalsy();

    component.form.controls['firstName'].setValue('foo');
    component.form.controls['lastName'].setValue('baz');
    component.form.controls['email'].setValue('test@test.com');
    component.form.controls['password'].setValue('ABc123456789');
    component.form.controls['confirmPassword'].setValue('ABc123456789');

    expect(component.form.valid).toBeTruthy();

    component.onSubmit();

    const req = httpMock.expectOne('https://demo-api.now.sh/users');
    req.flush('404 error', { status: 404, statusText: 'Not Found' });

    expect(component.signUpInvalid).toBe(true);

    httpMock.verify();
  });

  it('should set formSubmitAttempt to true if the form is not valid', () => {
    expect(component.form.valid).toBeFalsy();

    component.onSubmit();

    expect(component.formSubmitAttempt).toBe(true);
  });
});

