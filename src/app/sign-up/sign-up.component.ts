import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MustMatch } from '../_helpers/must-match.validator';
import { ShouldNotContain } from '../_helpers/should-not-contain.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})

export class SignUpComponent implements OnInit {
  SERVER_URL = 'https://demo-api.now.sh/users';
  contactForm: FormGroup;

  form: FormGroup;
  public signUpInvalid: boolean;
  public signUpSuccess: boolean;
  public formSubmitAttempt: boolean;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
  ) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [
          Validators.required,
          Validators.email,
        ]
      ],
      password: ['', [
        Validators.required,
        Validators.pattern('(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}'),
      ]],
      confirmPassword: ['', Validators.required]
    }, {
      validators: [
        ShouldNotContain('password'),
        MustMatch('password', 'confirmPassword'),
      ]
    });
  }

  onSubmit() {
    const formData = new FormData();

    this.signUpInvalid = false;
    this.signUpSuccess = false;
    this.formSubmitAttempt = false;

    if (this.form.valid) {
      this.httpClient.post<any>(this.SERVER_URL, formData).subscribe( (res) => {
          this.signUpSuccess = true;
        },
        (err) => {
          this.signUpInvalid = true;
          return console.log(err);
        }
      );
    } else {
      this.formSubmitAttempt = true;
    }
  }
}
