import { FormGroup } from '@angular/forms';

export function ShouldNotContain(field: string) {
    return (formGroup: FormGroup) => {
        const fieldItem = formGroup.controls[field];
        const firstName = formGroup.controls['firstName'];
        const lastName = formGroup.controls['lastName'];

        const fieldValue = fieldItem.value.toLowerCase();
        const firstNameValue = firstName.value.toLowerCase();
        const lastNameValue = lastName.value.toLowerCase();

        if (fieldItem.errors && !fieldItem.errors.shouldNotContain) {
            return;
        }

        // check if the field name includes the values of the first or last name
        if (fieldValue.includes(firstNameValue) || fieldValue.includes(lastNameValue)) {
          fieldItem.setErrors({ shouldNotContain: true });
        } else {
          fieldItem.setErrors(null);
        }
    }
}
